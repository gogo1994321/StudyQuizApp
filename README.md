This small program was created so that I could write together lots of math problems for my small brother and test him efficiently. 
You can easily load in a txt file that is specially formatted to ask questions and have the user answer them.

All problems have to be a single line in the txt-file. You have to divide up the different parts with '#' character in the following way:

    1+2#3

    5*6#30
    
You can also add extra info after the answer that has to be given, in case you are asking for example measurement conversion:

    500cm#5#m

    1L#1000#ml
    
There are several checkboxes that can be individually selected:

    Random - Instead of going down the lines of the .txt file we select the next problem randomly

    Non-Repeating - All problems appear only once (if they were solved correctly)

    One-Chance - If you give the wrong solution, you get a new question. Normally you can try as many times as you want to.
    
Try the newest build from this link: https://gitlab.com/gogo1994321/StudyQuizApp/blob/master/Build/StudyQuizApp.exe
    
