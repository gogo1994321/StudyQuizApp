﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StudyQuizApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        App app;

        public MainWindow()
        {
            InitializeComponent();
            app = ((App)Application.Current);
        }

        private void InputLoadButton_Click(object sender, RoutedEventArgs e)
        {
            //Try to write out previous Stats if there was a successfully loaded file
            if (!String.IsNullOrEmpty(app.fileName))
            {
                app.WriteOutStatistics();
                app.WriteSaveFile();
            }

            if (app.LoadQuizFile())
            {
                Task task = app.GetNewTask(false);
                ProblemText.Content = task.problem; //Select first problem
                extraInfoLabel.Content = task.extraInfo;
                ProgressBar.Maximum = app.TasksLeft(true);
                ProgressBar.Value = 0;
                SolutionBox.Text = "";
                CorrectSolutionText.Content = "";
                FileLabel.Content = app.fileName;
            }
            else
            {
                FileLabel.Content = "File doesn't seem to exist! Did you forget the '.txt' ?";
            }
        }

        private void RandomCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            app.selectRandomly = !app.selectRandomly;
        }

        private void NonRepeatingCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            app.nonRepeating = !app.nonRepeating;
        }

        private void SolutionButton_Click(object sender, RoutedEventArgs e)
        {
            if (app.TestSolution(SolutionBox.Text))
            {
                Task task = app.GetNewTask(true);
                ProblemText.Content = task.problem; //Select new problem
                extraInfoLabel.Content = task.extraInfo;
                ProgressBar.Value = app.TasksLeft(false);
                PerLabel.Content = app.TasksLeft(false)+"/"+app.TasksLeft(true);
                SolutionBox.Text = "";
            }
            else if(app.oneChance){
                Task task = app.GetNewTask(false);
                ProblemText.Content = task.problem; //Select new problem
                extraInfoLabel.Content = task.extraInfo;
                ProgressBar.Value = app.TasksLeft(false);
                PerLabel.Content = app.TasksLeft(false) + "/" + app.TasksLeft(true);
                SolutionBox.Text = "";
            }
        }

        private void NextTaskButton_Click(object sender, RoutedEventArgs e)
        {
            Task task = app.GetNewTask(false);
            ProblemText.Content = task.problem; //Select new problem
            extraInfoLabel.Content = task.extraInfo;
            app.IncreaseGiveUps();
        }

        private void GiveUpButton_Click(object sender, RoutedEventArgs e)
        {
            CorrectSolutionText.Content = app.GetSolution();
        }

        private void OneChanceCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            app.oneChance = !app.oneChance;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!String.IsNullOrEmpty(app.fileName))
            {
                app.WriteOutStatistics();
                app.WriteSaveFile();
            }
        }

        private void ForceWriteStats_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(app.fileName))
            {
                app.WriteOutStatistics();
                app.WriteSaveFile();
            }
        }
    }
}
