﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.IO;
using System.Diagnostics;

namespace StudyQuizApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>

    public class Task{
        public string problem { get; private set; }
        public string solution { get; private set; }
        public string extraInfo { get; private set; }
        public int occurences;  //How many times did the user try to solve this task? OR how many times did it appear

        public Task(string problem, string solution, string extraInfo = "") {
            this.solution = solution;
            this.problem = problem;
            this.extraInfo = extraInfo;
        }

        public override string ToString()
        {
            return problem + "#" + solution + "#"+extraInfo+"# Occurences:"+occurences;
        }
    }

    public partial class App : Application
    {
        List<Task> Tasks = new List<Task>();    //Holds all the read in tasks from file
        Task currentTask;
        List<int> indexesLeft = new List<int>();    //Holds all the indexes we didn't see yet. (Used to not repeat tasks)

        int currentTaskIndex = -1;
        public bool selectRandomly;
        public bool nonRepeating;
        public bool oneChance;
        public string fileName;
        int taskCount = 0;  //Amount of originally read from file Tasks

        //Statistics
        int amountOfCorrectSolutions;
        int amountOfWrongSolutions;
        int amountOfTotalTasks; //Needed since we might allow repeated questions, so taskCount isn't necessarily the amount of posed Tasks
        int amountOfGiveUps;

        string solvedTasks;
        string wronglyAnsweredTasks;

        Random r = new Random();

        public bool LoadQuizFile()
        {
            ResetVariables();
            string line;


            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".txt";
            dlg.Filter = "TXT Files (*.txt)|*.txt|All Files(*)|*.*";
            dlg.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;

            // Display OpenFileDialog by calling ShowDialog method 
            bool? result = dlg.ShowDialog();

            if (result == true)
            {

                try
                {
                    StreamReader file = new StreamReader(dlg.FileName);
                    fileName = dlg.SafeFileName;
                    while ((line = file.ReadLine()) != null)    //Read each line from the file and add it to the dictionary
                    {
                        if (String.IsNullOrWhiteSpace(line))
                        {
                            continue;
                        }
                        string[] splitLines = line.Split('#');
                        if (splitLines.Length >= 2)
                        {
                            if (splitLines.Length == 3)
                            {
                                Tasks.Add(new Task(splitLines[0], splitLines[1], splitLines[2]));
                            }
                            else
                            {
                                Tasks.Add(new Task(splitLines[0], splitLines[1]));
                            }
                            indexesLeft.Add(taskCount);
                            taskCount++;
                        }
                    }
                    file.Close();
                    return true;
                }
                catch
                {
                    // Write error.
                    Trace.WriteLine("There appears to be no file there. Path is: " + AppDomain.CurrentDomain.BaseDirectory);
                    return false;
                }
            }
            return false;
        }

        private void GetNewTaskIndex()
        {
            if (selectRandomly)
            {
                currentTaskIndex = indexesLeft[r.Next(indexesLeft.Count)];
            }
            else
            {
                currentTaskIndex++;
                if (currentTaskIndex >= indexesLeft.Count)
                {
                    currentTaskIndex = 0;
                }
                currentTaskIndex = indexesLeft[currentTaskIndex];
            }
        }

        public Task GetNewTask(bool solvedSolution) //Selects new task and returns the problem of it
        {
            if (Tasks.Count <= 0)
            {
                return new Task("Oh no, there are no Tasks in the file at all! o.O","");
            }

            GetNewTaskIndex();

            currentTask = Tasks[currentTaskIndex];

            if (nonRepeating && solvedSolution)
            {
                indexesLeft.Remove(currentTaskIndex);
            }
            
            if (indexesLeft.Count <= 0)
            {
                return new Task("You finished off all the Tasks! Load in a new file.","");
            }

            Trace.WriteLine("New task: " + currentTask.problem + "=" + currentTask.solution+" TaskCount: "+indexesLeft.Count);
            return currentTask;
        }

        public bool TestSolution(string solution)  //Check if provided solution is matching to the one in current Task
        {
            if (currentTask != null)
            {
                bool solved = solution.Trim() == currentTask.solution.Trim();
                currentTask.occurences++;
                if (solved)
                {
                    amountOfCorrectSolutions++;
                    if(String.IsNullOrWhiteSpace(solvedTasks) || !solvedTasks.Contains(currentTask.ToString()))
                        solvedTasks += currentTask.ToString()+"\n";
                    //Trace.WriteLine(solvedTasks);
                }
                else
                {
                    amountOfWrongSolutions++;
                    if (String.IsNullOrWhiteSpace(wronglyAnsweredTasks) || !wronglyAnsweredTasks.Contains(currentTask.ToString()))
                        wronglyAnsweredTasks += currentTask.ToString() + "\n";
                }

                amountOfTotalTasks++;

                return solved;
            }
            return false;
        }

        public string GetSolution()
        {
            return currentTask != null ? currentTask.solution : "";
        }

        public void IncreaseGiveUps() {
            amountOfGiveUps++;
            amountOfTotalTasks++;
        }

        public double TasksLeft(bool max)   //Used to set the progressbar to correct fill
        {
            return max ? Tasks.Count : taskCount - indexesLeft.Count;
        }

        public void WriteOutStatistics()
        {

            string path = AppDomain.CurrentDomain.BaseDirectory+"Stats.txt";
            
            TextWriter tw = new StreamWriter(path,true);
            tw.WriteLine(DateTime.Now+"  =====  "+fileName);
            tw.WriteLine("Amount of questions in the file: "+taskCount);
            tw.WriteLine("Amount of questions asked: "+amountOfTotalTasks);
            tw.WriteLine("Amount of correct answers: "+amountOfCorrectSolutions+" = "+(amountOfTotalTasks>0 ? ((1f*amountOfCorrectSolutions/amountOfTotalTasks)*100f) : 0)+"%");
            tw.WriteLine("Amount of wrong answers: " + amountOfWrongSolutions + " = " + (amountOfTotalTasks > 0 ? ((1f * amountOfWrongSolutions / amountOfTotalTasks) * 100f) : 0) + "%");
            tw.WriteLine("Amount of given up questions: "+amountOfGiveUps + " = " + (amountOfTotalTasks > 0 ? ((1f * amountOfGiveUps / amountOfTotalTasks) * 100f) : 0) + "%");
            tw.WriteLine("<----------------------------------------------------->");
            tw.WriteLine("Correctly solved questions:");

            if (!String.IsNullOrWhiteSpace(solvedTasks))
            {
                string[] holder = solvedTasks.Split('\n');
                foreach (string s in holder)
                {
                    tw.WriteLine(s);
                }
            }
            tw.WriteLine("<----------------------------------------------------->");
            tw.WriteLine("Incorrectly answered questions:");
            if (!String.IsNullOrWhiteSpace(wronglyAnsweredTasks))
            {
                string[] holder = wronglyAnsweredTasks.Split('\n');
                foreach (string s in holder)
                {
                    tw.WriteLine(s);
                }
            }
            tw.WriteLine("<=====================================================>");
            tw.Close();
        }

        public void WriteSaveFile()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Save_"+(fileName.Contains("Save_") ? fileName.Substring(5) : fileName);

            TextWriter tw = new StreamWriter(path, false);

            foreach (Task task in Tasks)
            {
                if(!solvedTasks.Contains(task.ToString()))
                    tw.WriteLine(task.ToString());
            }

            tw.Close();
        }

        private void ResetVariables()
        {
            solvedTasks = String.Empty;
            wronglyAnsweredTasks = String.Empty;
            fileName = String.Empty;
            taskCount = 0;
            indexesLeft.Clear();
            Tasks.Clear();
            currentTaskIndex = -1;
            amountOfCorrectSolutions = 0;
            amountOfGiveUps = 0;
            amountOfTotalTasks = 0;
            amountOfWrongSolutions = 0;
        }
    }
}
